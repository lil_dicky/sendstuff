import java.awt.Desktop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;  
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

/**
 * GUI Code for both server and client (receiver and sender, respectively).
 * Contains code from two classes not written by me:
 * 	-> PlatformHelper: 	A smaller helper function (essentially just an if else statement) designed to update JavaFX
 * 						objects on their correct thread
 *  -> DesktopApi: 	A supplement class for Desktop which cannot be installed on the NARGA machines. Anywhere I referenced
 *  				this class, I left a commented version of the standard Java code if it is so wished to be used. It is
 *  				basically just used to open the picture quickly from the GUI without having to go to the command line/ finder.
 * 
 * @author 18322166
 *
 */
public class GUI extends Application {
	
  public static final CountDownLatch latch = new CountDownLatch(1);
  public static GUI timingGUI = null;
  
  private Scene scene;
  private Stage stage;
  
  private File toSend;
  
  public enum LOGIN_TYPE {
	  LOGIN,
	  REGISTER
  }
  
  public LOGIN_TYPE type = LOGIN_TYPE.LOGIN;
  
  public boolean btnPressed;
  
  /**
   * Concurrency in JavaFX
   * 
   * @return
   */
  public static GUI startTiming(boolean type) {
	  isReciever = type;
      try {
          latch.await();
      } catch (Exception e) {
          System.err.println("Error starting GUI");
      }
      return timingGUI; 
  }

  /**
   * Initializes latch
   * 
   * @param timing
   */
  public static void setupTiming(GUI timing) {
      timingGUI = timing;
      latch.countDown();
  }

  /**
   * Constructor
   */
  public GUI() {
      setupTiming(this);
  }
  
  private static boolean isReciever = false;
  
  /**
   * Sets up stage & consequent default GUI panes for startup
   * 
   * @param stage
   * @throws Exception
   */
  public void start(final Stage stage) throws Exception {
		stage.setTitle("poor mans utorrent");
		this.stage = stage;
		init();
  }
  
  /**
   * Initialises GUI for either receiver or sender, depending on the value of isReciever (as set earlier)
   */
  public void init() {
	  BorderPane layout;
	  if (isReciever == true) layout = reciever();
	  else layout = sender();
	  
	  try {
		    scene = new Scene(layout);
		    stage.setScene(scene);
		    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		        @Override
		        public void handle(WindowEvent t) {
		            Platform.exit();
		            System.exit(0);
		        }
		    });
		    stage.show();
	    } catch (Exception e) {}
  }
  
  /**
   * GUI Pane for reciever
   * 
   * @return
   */
  private BorderPane reciever() {
	  
	  FlowPane main = new FlowPane(Orientation.VERTICAL, 5, 5);
	  main.setPadding(new Insets(5));
	  Label fileStatus = new Label("No files currently being sent");
	  
      final Button saveBtn = new Button("Open");
      saveBtn.setDisable(true);

      saveBtn.setOnAction(event -> {
    	  try {
    		  //Desktop.getDesktop().open(new File(Server.fileName));
    		  DesktopApi.open(new File(Server.fileName));
		} catch (Exception e) {
			System.err.println("Sorry, I can't seem to open this.\nPerhaps the file type is unreadable by this system?");
		}
      });
      
      Task<Integer> task = new Task<Integer>() {
		    @Override protected Integer call() throws Exception {
		        while(true) {
		        	PlatformHelper.run(() -> {
		        		switch (Server.statuss) {
			        	case INCOMING:  fileStatus.setText("File Incoming");
		        						saveBtn.setDisable(true);
		        						break;
			        	case IDLE:  	fileStatus.setText("Waiting for files to be sent");
										saveBtn.setDisable(true);
										break;
			        	case RECIEVED:  fileStatus.setText("File "+Server.fileName+" Recieved"+Server.viaType);
										saveBtn.setDisable(false);
										break;
			        	case ERROR:  	fileStatus.setText("Error recieving file. Please try again");
										saveBtn.setDisable(true);
										break;
			        	}
		        		
		        	});
		        	Thread.sleep(1000); 
		        }
		    }
		};
		new Thread(task).start();
	  
	  	FlowPane progress = new FlowPane(Orientation.HORIZONTAL, 5, 5);
		progress.setPadding(new Insets(5));
		Label pt = new Label("Progress ->");
		ProgressBar pb = new ProgressBar(0);
		ProgressIndicator pi = new ProgressIndicator(0);
		progress.getChildren().addAll(pt, pb, pi);
		
		Task<Integer> task2 = new Task<Integer>() {
		    @Override protected Integer call() throws Exception {
		        while(true) {
		        	PlatformHelper.run(() -> {
		        		pb.setProgress(Server.percDone);
		                pi.setProgress(Server.percDone);
		        	});
		        	Thread.sleep(10); 
		        }
		    }
		};
		new Thread(task2).start();
		
	    //main scene
	    BorderPane root = new BorderPane();
	    root.setBottom(progress);
	    root.setCenter(fileStatus);
	    root.setTop(saveBtn);
	    root.setPrefSize(400, 300);

	    return root;
  }
  
  /**
   * GUI Pane for sender
   * 
   * @return
   */
  private BorderPane sender() {
	  
	  Button sendBtnRBUDP = new Button("RBUDP");
	  sendBtnRBUDP.setDisable(true);
	  sendBtnRBUDP.setOnAction(event -> {
		  ClientSocket.type = ClientSocket.PROTOCOL.RBUDP;
		  ClientSocket.toSend = toSend;
      });
	  
	  Button sendBtnTCP = new Button("TCP");
	  sendBtnTCP.setDisable(true);
	  sendBtnTCP.setOnAction(event -> {
		  ClientSocket.type = ClientSocket.PROTOCOL.TCP;
		  ClientSocket.toSend = toSend;
      });

	  final FileChooser fileChooser = new FileChooser();
	  
      final Button openButton = new Button("Select File");

      openButton.setOnAction(new EventHandler<ActionEvent>() {
              @Override
              public void handle(final ActionEvent e) {
                  File file = fileChooser.showOpenDialog(stage);
                  if (file != null) {
                	  sendBtnTCP.setDisable(false);
                	  sendBtnRBUDP.setDisable(false);
                	  toSend = file;
                  }
              }
          });
		
      	FlowPane sendFile = new FlowPane(Orientation.HORIZONTAL, 5, 5);
      	sendFile.setPadding(new Insets(5));
		Label pt = new Label("Send File");
		sendFile.getChildren().addAll(pt, sendBtnRBUDP, sendBtnTCP);
	    
	    //main scene
	    BorderPane root = new BorderPane();
	    root.setBottom(sendFile);
	    root.setCenter(openButton);
	    root.setPrefSize(400, 300);

	    return root;
  }
  
}
