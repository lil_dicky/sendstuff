/**
 * Global parameters
 * 
 * @author 18322166
 *
 */
public class Params {
	public final static int PORT_NUM = 8000;  
	public static String IP = "localhost";
	public final static int MAX_FILE_BUFFER = 1024;
	public final static int BLOCK_SIZE = 5;
}
     