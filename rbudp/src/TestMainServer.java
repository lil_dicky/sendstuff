import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Run Receiver
 * 
 * @author 18322166
 *
 */
public class TestMainServer { 
	
	/**
	 * Starts necessary threads
	 * 
	 * @param args
	 * @throws UnknownHostException
	 */
	public static void main(String[] args) throws UnknownHostException {
		 Thread serving = serverThread(Params.PORT_NUM);
		 System.out.println(InetAddress.getLocalHost());
		 serving.start();
	}

	/**
	 * Concurrent server with GUI
	 * 
	 * @param portNum
	 * @return
	 */
	public static Thread serverThread(final int portNum){
		Thread thread = new Thread() {
			public void run() {
				try (
				Server server = new Server(portNum);
				){
					server.go();
				}catch(IOException e) {
					System.err.printf("Could not connect to port %d or establish a connection", portNum);
				} 
			}
		}; 
		return thread;
	}
}
