import java.io.Serializable;

/**
 * Object that carries important information to correctly assign and organise files received on the server side with information
 * only available to the client.
 * 
 * @author 18322166
 *
 */
public class FileDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private ClientSocket.PROTOCOL type;
	private long size;
	private byte[] buf;
	
	public FileDetails(String name, ClientSocket.PROTOCOL type, long size) {
		this.name = name;
		this.type = type;
		this.size = size;
	}
	
	public FileDetails(String name, ClientSocket.PROTOCOL type, long size, byte[] buf) {
		this.name = name;
		this.type = type;
		this.size = size;
		this.buf = buf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ClientSocket.PROTOCOL getType() {
		return type;
	}

	public void setType(ClientSocket.PROTOCOL type) {
		this.type = type;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public byte[] getBuf() {
		return buf;
	}

	public void setBuf(byte[] buf) {
		this.buf = buf;
	}

}
