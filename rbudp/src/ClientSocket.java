import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;

/**
 * Main code for Client Thread
 * 
 * @author 18322166
 *
 */
public class ClientSocket extends Socket{
	private String hostName;
	private int portNum;
	
	public static File toSend;
	
	public enum PROTOCOL {
		  RBUDP,
		  TCP;
	  }
	
	public static PROTOCOL type;
	
	private Socket clientSocket = null;
	public ClientSocket(String hostName, int portNum) throws IOException {
		this.hostName = hostName;
		this.portNum = portNum;
	}
	public static HashSet<String> users = new HashSet<String>();
	public static GUI gui;
	
	/**
	 * Initalises gui and consequent network communication by user
	 */
	public void go()  {
		try {
			
			new Thread() {
			    @Override
			    public void run() {
			        javafx.application.Application.launch(GUI.class);
			    }
			}.start();
			
			gui = GUI.startTiming(false);
			
			while (true) {
				Socket clientSocket = new Socket(hostName, portNum);
			
				while (toSend == null) { System.out.print(""); }
				
				ObjectOutputStream objOut = new ObjectOutputStream(clientSocket.getOutputStream());
				FileDetails details = new FileDetails(toSend.getName(), type, toSend.length());
				objOut.writeObject(details);
				
				byte buf[] = new byte[Params.MAX_FILE_BUFFER];
				int read = 0;
			    switch (type) {
				case RBUDP:
					DatagramSocket inDG = new DatagramSocket();
					InputStream in2 = new FileInputStream(toSend.getAbsolutePath());
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					
					int inc = 0;
				    for (int i = 0; i < Params.BLOCK_SIZE; i++ ) {
				        if ((read = in2.read(buf)) < 0) break;
				        outputStream.write(buf, 0, read); 
				        inc = inc + read;
				    }
				    
					DatagramPacket sendPacket = new DatagramPacket(outputStream.toByteArray(), outputStream.toByteArray().length, InetAddress.getByName(hostName), portNum);
					inDG.send(sendPacket);
					
					break;
				case TCP:
				    InputStream in = new FileInputStream(toSend.getAbsolutePath());
					OutputStream out = clientSocket.getOutputStream();
				    while ((read = in.read(buf)) >= 0) {
				        if (read < 0)	break;
				        out.write(buf, 0, read);
				    }
				    out.close();
				    in.close();
					break;
			    }
			    
			    toSend = null;
			    clientSocket.close();
			}
			
		} catch(UnknownHostException e) {
			System.err.printf("Host %s is unknown", hostName);
			System.exit(1);
		} catch(IOException e) {
			System.err.printf("Could not get I/O for the connection to host: %s", hostName);
			System.exit(1);
		} finally {
			if(clientSocket != null) {
				try {
					clientSocket.close();
				} catch(IOException e) {
					System.err.println("Could not get I/O for the connection");
					System.exit(1);
				}
			
			} 
		}
	}
	
}
