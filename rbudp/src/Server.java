import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Main code for server thread
 * 
 * @author 18322166
 *
 */
public class Server extends ServerSocket{
	int portNum;
	public static GUI gui;
	
	public static FILE_STATUS statuss = FILE_STATUS.IDLE;
	public enum FILE_STATUS {
		  INCOMING,
		  IDLE,
		  RECIEVED,
		  ERROR;
	  }
	
	//the following three variables are used by the gui thread on the reciever side to indicate whether a file has been recieved
	public static String fileName = "";
	public static String viaType = "";
	public static double percDone = 0;
	
	public Server(int portNum) throws IOException{
		this.portNum= portNum;			
	}
	
	/**
	 * Initalises gui and consequent network communication by user
	 */
	public void go() {
		try {
			ServerSocket serverSocket = new ServerSocket(portNum);
			
			new Thread() {
			    @Override
			    public void run() {
			        javafx.application.Application.launch(GUI.class);
			    }
			}.start();

			gui = GUI.startTiming(true);
			
			while (true) {
				
				Socket clientSocket = serverSocket.accept();
				
				byte buf[] = new byte[Params.MAX_FILE_BUFFER];
				int inc = 0;
				int read = 0;
			    
			    ObjectInputStream objIn = new ObjectInputStream(clientSocket.getInputStream());
				FileDetails details = (FileDetails) objIn.readObject();
				fileName = details.getName();
				
			    switch (details.getType()) {
				case RBUDP:
					viaType = " via RBUDP";
					statuss = FILE_STATUS.INCOMING;
					
					DatagramSocket outDG = new DatagramSocket(portNum);
					DatagramPacket recvPacket = new DatagramPacket(buf, buf.length);
				    outDG.receive(recvPacket);
				    
				    ByteArrayInputStream inputStream = new ByteArrayInputStream(recvPacket.getData());
					
				    OutputStream out2 = new FileOutputStream(details.getName());
				    while ((read = inputStream.read(buf)) >= 0) {
				        out2.write(buf, 0, read);
				        inc = inc + read;
				    }
				    outDG.close(); 
					
					break;
				case TCP:
					viaType = " via TCP";
					OutputStream out = new FileOutputStream(details.getName());
					statuss = FILE_STATUS.INCOMING;
				    InputStream inTCP = clientSocket.getInputStream();
				    while ((read = inTCP.read(buf)) >= 0) {
				        out.write(buf, 0, read);
				        inc = inc + read;
				        percDone = ((double)(inc) / (double)(details.getSize()));
				    }
				    out.close();
				    inTCP.close();
					break;
			    }
				statuss = FILE_STATUS.RECIEVED;
				
			}
			
		} catch(IOException e) {
			System.err.printf("Exception caught when trying to connect to port %d or trying to establish a connection: %s \n",
					portNum, e.getMessage());
		} catch (ClassNotFoundException e) {
			System.err.println("Couldnt find object");
		}
	}
}