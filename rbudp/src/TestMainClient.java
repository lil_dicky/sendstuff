import java.io.IOException;
import java.net.UnknownHostException;
   
/**
 * Run Sender
 * 
 * @author 18322166
 *
 */
public class TestMainClient {
	
	/**
	 * Starts necessary threads
	 * 
	 * @param args
	 * @throws UnknownHostException
	 */
	public static void main(String[] args) throws UnknownHostException {
		if (args.length > 0) Params.IP = args[0];
		 Thread served = clientThread(Params.IP, Params.PORT_NUM);
		 served.start();
	}
	
	/**
	 * Concurrent client with GUI
	 * 
	 * @param hostName
	 * @param portNum
	 * @return
	 */
	public static Thread clientThread(final String hostName, final int portNum) {
		Thread thread = new Thread() {
			public void run() {
				try (
					ClientSocket client = new ClientSocket(hostName, portNum); 
				){
					client.go();
				} catch(IOException e) {
					System.err.printf("Unknown host: %d  \n", hostName);
				}
			}
		};
		return thread;
	}
}
